window.MathJax = {
    TeX: {
        extensions: ["AMSmath.js", "AMSsymbols.js", "mediawiki-texvc.js"]
    }
};