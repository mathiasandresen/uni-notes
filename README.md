# Introduction

![Build Status](https://gitlab.com/pages/mkdocs/badges/master/build.svg)

This project's static site is hosted by [GitLab Pages][pages] and built by [GitLab CI][ci].


### Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][mkdocs-install] MkDocs
1. Preview your project: `mkdocs serve`,
   your site can be accessed under `localhost:8000`
1. Add content
1. Generate the website: `mkdocs build` (optional)

Read more at [MkDocs].

### MarkDown Cheatsheet

https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#images


### MkDocs Themes
https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes 

---

_Originally forked from https://gitlab.com/morph027/mkdocs_

[pages]: https://pages.gitlab.io
[ci]: https://about.gitlab.com/gitlab-ci/
[mkdocs]: https://www.mkdocs.org
[mkdocs-install]: https://www.mkdocs.org/#installation